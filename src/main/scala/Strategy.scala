object Strategy extends App {
  def calculateFedEx(orderPrice: Int) = 5 * orderPrice
  def calculateUPS(orderPrice: Int) = 4.25 * orderPrice 
  def calculateSchenker(orderPrice: Int) = 3 * orderPrice

  def executeStrategy(callback: (Int) => Double, orderPrice: Int) = callback(orderPrice)

  println("FedEx: " + executeStrategy(calculateFedEx, 20))
  println("UPS: " + executeStrategy(calculateUPS, 20))
  println("Schenker: " + executeStrategy(calculateSchenker, 20))
}