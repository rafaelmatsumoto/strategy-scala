class StrategyTest extends org.scalatest.funsuite.AnyFunSuite {
    test("Strategy.executeStrategy") {
        assert(Strategy.executeStrategy(Strategy.calculateFedEx, 20) === 100)
        assert(Strategy.executeStrategy(Strategy.calculateUPS, 20) === 85)
        assert(Strategy.executeStrategy(Strategy.calculateSchenker, 20) === 60)
    }
}