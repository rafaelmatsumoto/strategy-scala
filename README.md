# Strategy

Implementação do padrão Strategy em Scala

## Pré-requisitos

- [Scala](https://scala-lang.org/)
- [Sbt](https://www.scala-sbt.org/)

## Utilização

- Rodar a aplicação

```bash
sbt
```

- Rodar os testes

```bash
sbt test
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
